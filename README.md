Diceware4Bash
==========

What is this all about?
-----
This project provides a Bash script to generate secure passphrases from the command line using a Diceware word list.

What is Diceware?
-----
Diceware is a technique for generating secure passphrases using dice rolls. This technique was created by Arnold G. Reinhold and documented in full at <http://world.std.com/~reinhold/dicewarefaq.html>

***WARNING***

As pointed out in the URL above, the Diceware method should really be conducting using a truly random source, such as casino-grade dice. The use of electronic random number generation ***may*** not provide truly random numbers which (if true) would compromise the security of any generated passphrases.

This project is provided without warranty of any kind. Using this script to generate passphrases is entirely at the users own risk.

How do I use it?
-----
* Add `diceware` script to your path. I keep mine in `~/my_scripts/`
* Ensure that the `diceware` script is executable: `chmod u+x diceware`
* Download your chosen wordlist. For example: <http://world.std.com/~reinhold/beale.wordlist.asc>
* Set the location of the selected diceware wordlist by exporting the environment variable `DICEWARE_WORDLIST` in your `~/.bashrc` file (i.e. `export DICEWARE_WORDLIST=~/Documents/beale.wordlist.asc`).

```
$ diceware -h
Usage: diceware [OPTIONS]
Options:
	-h|--help|--usage
		Show this help
	-l|--length LENGTH
		Specify passphrase length in words (default: 6)
	-v|--verbose
		Increase verbosity
```


